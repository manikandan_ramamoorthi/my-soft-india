# My-Soft-India

My-Soft-India is Completely Hotel management  application, it helps to booking, reserversion, etc, with all feature with flexible user interface

**Developemet Dependencies**

| Name    | Version| Checking |
| ------  | ------ | ------   |
| Node Js | 14.5.5 | node --version |
| Angular | 11.2.0 | ng --version |
| Git     | any    | git --version |


**Step to run an application**

> Note : Make sure git is installed in your system 

- **SETP 1 :** Clone the Repository - git clone https://gitlab.com/manikandan_ramamoorthi/my-soft-india.git

- **SETP 2 :** Navigate to cloned Repository Floder and Open Terminal

- **SETP 3 :** run npm install command for download the libiray files from cloud

- **SETP 4 :** run npm start or ng serve



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

