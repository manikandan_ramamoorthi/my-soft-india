import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { AdminReducer } from './modules/admin/admin-store/admin-reducer';
import { FrontOfficeReducer } from './modules/front-office/front-office-store/front-office-reducer';
import { IAppState } from './app.state';

export const reducers: ActionReducerMap<IAppState> = {
  AdminReducer,
  FrontOfficeReducer,
};

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => reducer(state, action);
}

export const metaReducers: MetaReducer<any>[] = [debug];
