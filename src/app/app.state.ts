import { IAdminState } from './modules/admin/admin-store/admin-model';
import { IFrontOfficeState } from './modules/front-office/front-office-store/front-office-model';
export interface IAppState {
  readonly AdminReducer: IAdminState;
  readonly FrontOfficeReducer: IFrontOfficeState;
}
