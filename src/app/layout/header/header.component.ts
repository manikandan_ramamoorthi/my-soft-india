import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {
  LANGUAGES,
  LANG_KEY,
  TRANSLATE_KEYS,
} from 'src/app/shared/constants/language';
import { LocalStorageUtils } from 'src/app/shared/utils/localstorage-utils';
import { LayoutService } from '../layout.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  isCollapsed: boolean = false;
  subscriptionStack: Subscription[] = [];
  languageList: Array<string> = LANGUAGES;
  selectedLanguage: string;
  translateKey: string = TRANSLATE_KEYS.NAVIGATION.HEADER_BAR;

  constructor(
    private layoutService: LayoutService,
    private translateService: TranslateService
  ) {
    this.selectedLanguage = LocalStorageUtils.getValue(LANG_KEY) || 'en';
  }
  navigationToggle() {
    this.layoutService.setNavigationViewState(!this.isCollapsed);
  }
  handleLanguageChange(event: any) {
    LocalStorageUtils.setValue(LANG_KEY, this.selectedLanguage);
    this.translateService.use(this.selectedLanguage);
    location.reload();
  }

  ngOnInit(): void {
    const navigationViewStateSub = this.layoutService.navigationViewState.subscribe(
      (viewState) => (this.isCollapsed = viewState)
    );
    this.subscriptionStack.push(navigationViewStateSub);
  }
  ngOnDestroy() {
    this.subscriptionStack.forEach((sub) => sub.unsubscribe());
  }
}
