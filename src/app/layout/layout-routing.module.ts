import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/dashboard',
      },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('../modules/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('../modules/admin/admin.module').then((m) => m.AdminModule),
      },
      {
        path: 'banquets',
        loadChildren: () =>
          import('../modules/banquets/banquets.module').then(
            (m) => m.BanquetsModule
          ),
      },
      {
        path: 'food-costing',
        loadChildren: () =>
          import('../modules/food-costing/food-costing.module').then(
            (m) => m.FoodCostingModule
          ),
      },
      {
        path: 'front-office',
        loadChildren: () =>
          import('../modules/front-office/front-office.module').then(
            (m) => m.FrontOfficeModule
          ),
      },
      {
        path: 'house-keeping',
        loadChildren: () =>
          import('../modules/house-keeping/house-keeping.module').then(
            (m) => m.HouseKeepingModule
          ),
      },
      {
        path: 'maintenance',
        loadChildren: () =>
          import('../modules/maintenance/maintenance.module').then(
            (m) => m.MaintenanceModule
          ),
      },
      {
        path: 'material',
        loadChildren: () =>
          import('../modules/material/material.module').then(
            (m) => m.MaterialModule
          ),
      },
      {
        path: 'payroll',
        loadChildren: () =>
          import('../modules/payroll/payroll.module').then(
            (m) => m.PayrollModule
          ),
      },
      {
        path: 'point-of-sale',
        loadChildren: () =>
          import('../modules/point-of-sale/point-of-sale.module').then(
            (m) => m.PointOfSaleModule
          ),
      },
      {
        path: 'accounts',
        loadChildren: () =>
          import('../modules/accounts/accounts.module').then(
            (m) => m.AccountsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}
