import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LayoutService } from './layout.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
  isCollapsed: boolean = false;
  subscriptionStack: Subscription[] = [];
  isLoading: boolean = false;
  constructor(private layoutService: LayoutService, private router: Router) {}

  ngOnInit(): void {
    const navigationViewStateSub = this.layoutService.navigationViewState.subscribe(
      (viewState) => (this.isCollapsed = viewState)
    );
    this.subscriptionStack.push(navigationViewStateSub);
    const routerEventSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.isLoading = true;
      }
      if (event instanceof NavigationEnd) {
        this.isLoading = false;
      }
    });
    this.subscriptionStack.push(routerEventSub);
  }

  ngOnDestroy() {
    this.subscriptionStack.forEach((sub) => sub.unsubscribe());
  }
}
