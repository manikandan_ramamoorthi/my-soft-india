import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LayoutService {
  navigationViewState = new BehaviorSubject(true);
  constructor() {}
  setNavigationViewState(viewState: boolean) {
    console.log(viewState, 'viewState ');
    this.navigationViewState.next(viewState);
  }
}
