import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TRANSLATE_KEYS } from 'src/app/shared/constants/language';
import { LayoutService } from '../layout.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  isCollapsed: boolean = false;
  translateKey: string = TRANSLATE_KEYS.NAVIGATION.SIDE_BAR;
  isInitialTrigger: boolean = true;
  menuList: any = [
    { name: 'dashboard', icon: 'dashboard', route: '/dashboard' },
    { name: 'front_office', icon: 'smile', route: '/front-office' },
    { name: 'house_keeping', icon: 'clear', route: '/house-keeping' },
    { name: 'point_of_sale', icon: 'shop', route: '/point-of-sale' },
    { name: 'material', icon: 'audit', route: '/material' },
    { name: 'food_costing', icon: 'shopping', route: '/food-costing' },
    { name: 'banquets', icon: 'appstore', route: '/banquets' },
    { name: 'payroll', icon: 'file-done', route: '/payroll' },
    { name: 'admin', icon: 'user', route: '/admin' },
    { name: 'maintenance', icon: 'setting', route: '/maintenance' },
    { name: 'accounts', icon: 'team', route: '/accounts' }
  ];

  subscriptionStack: Subscription[] = [];

  constructor(private layoutService: LayoutService) {}

  handleCollapsedChanges(event: any) {
    if (!this.isInitialTrigger) {
      this.layoutService.setNavigationViewState(event);
    }
  }

  ngOnInit(): void {
    const navigationViewStateSub = this.layoutService.navigationViewState.subscribe(
      (viewState) => {
        this.isCollapsed = viewState;
        this.isInitialTrigger = false;
      }
    );
    this.subscriptionStack.push(navigationViewStateSub);
  }

  ngOnDestroy() {
    this.subscriptionStack.forEach((sub) => sub.unsubscribe());
  }
}
