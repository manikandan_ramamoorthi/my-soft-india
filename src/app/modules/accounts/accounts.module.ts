import { NgModule } from '@angular/core';
import { AccountsComponent } from './accounts.component';
import { AccountsRoutingModule } from './accounts-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [AccountsComponent],
  imports: [SharedModule, AccountsRoutingModule],
})
export class AccountsModule {}
