import { Actions } from './admin-action';
import { IAdminState } from './admin-model';

export const initialState: IAdminState = {};

export function AdminReducer(
  state = initialState,
  action: Actions
): IAdminState {
  return { ...state };
}
