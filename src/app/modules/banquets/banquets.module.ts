import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { BanquetsRoutingModule } from './banquets-routing.module';
import { BanquetsComponent } from './banquets.component';

@NgModule({
  declarations: [BanquetsComponent],
  imports: [SharedModule, BanquetsRoutingModule],
})
export class BanquetsModule {}
