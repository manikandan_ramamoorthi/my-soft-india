import { Component, OnInit } from '@angular/core';
import { TRANSLATE_KEYS } from 'src/app/shared/constants/language';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  translateKey: string = TRANSLATE_KEYS.DASHBOARD.INFO_CARD;
  infoCard: any = [
    { name: 'total_rooms', count: 40, icon: 'home', color: '#4682b4' },
    { name: 'check_in', count: 2, icon: 'login', color: '#34BFA3' },
    { name: 'check_out', count: 10, icon: 'logout', color: '#F24F7C' },
    { name: 'clean_rooms', count: 14, icon: 'fall', color: '#5ec312' },
    {
      name: 'dirty_rooms',
      count: 20,
      icon: 'issues-close',
      color: '#FFCC00',
    },
    { name: 'expected_arrival', count: 14, icon: 'fall', color: '#5ec312' },
    { name: 'expected_departure', count: 19, icon: 'rise', color: '#ff7f50' },
  ];
  constructor() {}

  ngOnInit(): void {}
  listOfData: toDoListI[] = [
    {
      isCompletes: true,
      title: 'Check Room Maintenance',
      desc: 'Please check the all room bed condition ',
      bookMark: true,
    },
    {
      isCompletes: true,
      title: 'Employee Meeting',
      desc: 'Group meeting for new year celebration',
      bookMark: true,
    },
    {
      isCompletes: true,
      title: 'Dinner #507',
      desc: 'Have to send dinner to Room No #507',
      bookMark: false,
    },
    {
      isCompletes: true,
      title: 'Water Problem',
      desc: 'Have to check water problem in Room No #302',
      bookMark: true,
    },
    {
      isCompletes: true,
      title: 'Check Room Maintenance',
      desc: 'Please check the all room bed condition ',
      bookMark: true,
    },
    {
      isCompletes: true,
      title: 'Employee Meeting',
      desc: 'Group meeting for new year celebration',
      bookMark: true,
    },
    {
      isCompletes: true,
      title: 'Dinner #507',
      desc: 'Have to send dinner to Room No #507',
      bookMark: false,
    },
    {
      isCompletes: true,
      title: 'Water Problem',
      desc: 'Have to check water problem in Room No #302',
      bookMark: true,
    },
  ];
  drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.listOfData, event.previousIndex, event.currentIndex);
  }
  editBookmark(value: boolean, index: number) {
    this.listOfData[index].bookMark = !value;
    // this.listOfData = [...this.listOfData];
  }
}

interface toDoListI {
  isCompletes: boolean;
  title: string;
  desc: string;
  bookMark: boolean;
}
