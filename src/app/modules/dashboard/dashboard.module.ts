import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
@NgModule({
  declarations: [DashboardComponent],
  imports: [SharedModule, DashboardRoutingModule, DragDropModule],
})
export class DashboardModule {}
