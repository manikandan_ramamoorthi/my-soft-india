import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodCostingComponent } from './food-costing.component';

const routes: Routes = [{ path: '', component: FoodCostingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodCostingRoutingModule {}
