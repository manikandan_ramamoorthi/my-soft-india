import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodCostingComponent } from './food-costing.component';

describe('FoodCostingComponent', () => {
  let component: FoodCostingComponent;
  let fixture: ComponentFixture<FoodCostingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodCostingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodCostingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
