import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { FoodCostingRoutingModule } from './food-costing-routing.module';
import { FoodCostingComponent } from './food-costing.component';

@NgModule({
  declarations: [FoodCostingComponent],
  imports: [SharedModule, FoodCostingRoutingModule],
})
export class FoodCostingModule {}
