import { generateSimpleTableConfig } from 'src/app/shared/components/simple-table/simple-table-config';
import {
  COLOR_TEXT_CLASS,
  TABLE_IDENTITY_KEYS,
  VISUAL_TYPES,
} from 'src/app/shared/components/simple-table/simple-table-enum';
import { ISimpleTableFullConfig } from 'src/app/shared/components/simple-table/simple-table-interface';
import { TRANSLATE_KEYS } from 'src/app/shared/constants/language';
const { FRONT_OFFICE } = TRANSLATE_KEYS;

const { COLOR_TEXT } = VISUAL_TYPES;

export const ROOM_AVAILABILITY_CONFIG: ISimpleTableFullConfig = {
  title: '',
  config: {
    date: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.SECONDARY,
    }),
    total_rooms: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.PURPLE,
    }),
    available_rooms: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.SUCCESS,
    }),
    arrival: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.PRIMARY,
    }),
    departure: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.DANGER,
    }),
    in_house: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.WARNING,
    }),
    mnc: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.DEEP_PINK,
    }),
    wl_tl: generateSimpleTableConfig({
      visualType: COLOR_TEXT,
      colorType: COLOR_TEXT_CLASS.PURPLE,
    }),
  },
  translateKey: FRONT_OFFICE.ROOM_AVAILABLE_STATUS_TABLE,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_AVAILABLE_STATUS_TABLE,
  staticData: [],
  isBordered: true,
  isPagination: false,
  isCheckBox: false,
  dimensions: { x: null, y: null },
};
