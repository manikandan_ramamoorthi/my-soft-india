import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { ROOM_AVAILABILITY_CONFIG } from './room-availability-config';

@Component({
  selector: 'app-room-availability',
  templateUrl: './room-availability.component.html',
  styleUrls: ['./room-availability.component.scss'],
})
export class RoomAvailabilityComponent implements OnInit {
  @Input() isVisible: boolean = false;
  @Output() close = new EventEmitter<boolean>();
  selectMonth: any = moment().toISOString();
  selectWeek: any = moment().toISOString();
  roomType: string = 'all';
  roomTypesOptions = [
    { label: 'All', value: 'all' },
    { label: 'DLX', value: 'dlx' },
    { label: 'GV', value: 'gv' },
    { label: 'STD', value: 'std' },
  ];
  roomStatusConfig = ROOM_AVAILABILITY_CONFIG;
  roomAvailableColumns: any = [
    'title',
    'date',
    'date1',
    'date2',
    'date3',
    'date4',
    'date5',
    'date6',
    'date7',
    'date8',
    'date9',
    'date10',
    'date11',
    'date12',
    'date13',
    'date14',
  ];
  roomAvailabilityNewHeader: any = [
    {
      title: 'Day',
      date: 'Sun',
      date1: 'Mon',
      date2: 'Tue',
      date3: 'Wed',
      date4: 'Thu',
      date5: 'Fri',
      date6: 'Sat',
      date7: 'Sun',
      date8: 'Mon',
      date9: 'Tue',
      date10: 'Wed',
      date11: 'Thu',
      date12: 'Fri',
      date13: 'Sat',
      date14: 'Sun',
    },
    {
      title: 'Date',
      date: '01',
      date1: '02',
      date2: '03',
      date3: '04',
      date4: '05',
      date5: '06',
      date6: '07',
      date7: '08',
      date8: '09',
      date9: '10',
      date10: '11',
      date11: '12',
      date12: '13',
      date13: '14',
      date14: '15',
    },
  ];
  roomAvailabilityNew: any = [
    {
      title: 'Total',
      date: '45',
      date1: '45',
      date2: '45',
      date3: '45',
      date4: '45',
      date5: '45',
      date6: '45',
      date7: '45',
      date8: '45',
      date9: '45',
      date10: '45',
      date11: '45',
      date12: '45',
      date13: '45',
      date14: '45',
    },
    {
      title: 'ARR',
      date: '03',
      date1: '02',
      date2: '01',
      date3: '04',
      date4: '05',
      date5: '07',
      date6: '01',
      date7: '02',
      date8: '04',
      date9: '05',
      date10: '05',
      date11: '07',
      date12: '06',
      date13: '08',
      date14: '15',
    },
    {
      title: 'DEP',
      date: '05',
      date1: '04',
      date2: '01',
      date3: '07',
      date4: '08',
      date5: '04',
      date6: '06',
      date7: '05',
      date8: '04',
      date9: '03',
      date10: '00',
      date11: '05',
      date12: '04',
      date13: '15',
      date14: '04',
    },
    {
      title: 'INH',
      date: '02',
      date1: '05',
      date2: '04',
      date3: '15',
      date4: '06',
      date5: '35',
      date6: '08',
      date7: '03',
      date8: '05',
      date9: '08',
      date10: '01',
      date11: '04',
      date12: '00',
      date13: '01',
      date14: '05',
    },
    {
      title: 'BLK',
      date: '01',
      date1: '03',
      date2: '06',
      date3: '07',
      date4: '00',
      date5: '00',
      date6: '00',
      date7: '00',
      date8: '00',
      date9: '00',
      date10: '04',
      date11: '01',
      date12: '04',
      date13: '07',
      date14: '00',
    },
    {
      title: 'Avail',
      date: '30',
      date1: '26',
      date2: '18',
      date3: '19',
      date4: '37',
      date5: '39',
      date6: '25',
      date7: '29',
      date8: '33',
      date9: '44',
      date10: '42',
      date11: '29',
      date12: '27',
      date13: '39',
      date14: '40',
    },
  ];

  roomAvailableStatus = [
    {
      date: '1 / Mon',
      total_rooms: 51,
      available_rooms: 45,
      arrival: 12,
      departure: 4,
      in_house: 3,
      mnc: 4,
      wl_tl: 0,
    },
    {
      date: '1 / Mon',
      total_rooms: 51,
      available_rooms: 45,
      arrival: 12,
      departure: 4,
      in_house: 3,
      mnc: 4,
      wl_tl: 0,
    },
    {
      date: '1 / Mon',
      total_rooms: 51,
      available_rooms: 45,
      arrival: 12,
      departure: 4,
      in_house: 3,
      mnc: 4,
      wl_tl: 0,
    },
    {
      date: '1 / Mon',
      total_rooms: 51,
      available_rooms: 45,
      arrival: 12,
      departure: 4,
      in_house: 3,
      mnc: 4,
      wl_tl: 0,
    },
    {
      date: '1 / Mon',
      total_rooms: 51,
      available_rooms: 45,
      arrival: 12,
      departure: 4,
      in_house: 3,
      mnc: 4,
      wl_tl: 0,
    },
    {
      date: '1 / Mon',
      total_rooms: 51,
      available_rooms: 45,
      arrival: 12,
      departure: 4,
      in_house: 3,
      mnc: 4,
      wl_tl: 0,
    },
    {
      date: '1 / Mon',
      total_rooms: 51,
      available_rooms: 45,
      arrival: 12,
      departure: 4,
      in_house: 3,
      mnc: 4,
      wl_tl: 0,
    },
  ];

  constructor() {}

  mapDate() {
    // var currentDate = moment(this.selectMonth);
    // var monthStart = currentDate.clone().startOf('month').get('date');
    // var monthEnd = currentDate.clone().endOf('month').get('date');
    // console.log({ monthStart, monthEnd });
    // const dateMap: any = {};
    // for (let i = 0; i < monthEnd; i++) {
    //   const runningDate = moment(monthStart).add(i, 'days');
    //   const dateNumber = runningDate.get('date');
    //   const day = runningDate.format('ddd');
    //   dateMap[`_${dateNumber}`] = {
    //     day,
    //     dateNumber,
    //   };
    // }
    // console.log(dateMap, 'dateMap');

    // this.roomAvailabilityNew.map((item: any) => {

    //   console.log(item, '=>');
    // });
    const weekStart = moment(this.selectWeek).startOf('week');
    for (let i = 0; i <= 6; i++) {
      this.roomAvailableStatus[i].date = moment(weekStart)
        .add(i, 'days')
        .format('DD/MM/YY (ddd)');
    }
  }

  ngOnInit(): void {
    this.mapDate();
  }

  dateOnChange(event: any) {
    console.log(event, 'event');
  }

  handleCancel() {
    this.close.emit(false);
  }
}
