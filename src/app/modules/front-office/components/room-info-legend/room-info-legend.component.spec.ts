import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomInfoLegendComponent } from './room-info-legend.component';

describe('RoomInfoLegendComponent', () => {
  let component: RoomInfoLegendComponent;
  let fixture: ComponentFixture<RoomInfoLegendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoomInfoLegendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomInfoLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
