import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-info-legend',
  templateUrl: './room-info-legend.component.html',
  styleUrls: ['./room-info-legend.component.scss'],
})
export class RoomInfoLegendComponent implements OnInit {
  statusInfoData = [
    {
      label: 'Total Room',
      color: '#c0d700',
      count: 51,
    },
    {
      label: 'Vacant',
      color: '#00d700',
      count: 32,
    },
    {
      label: 'Dirty',
      color: '#ffff2b',
      count: 1,
    },
    {
      label: 'Occupied',
      color: '#ff3e3e',
      count: 14,
    },
    {
      label: 'Exp Dep',
      color: '#bb00ff',
      count: 3,
    },
    {
      label: 'Maintenance',
      color: '#c4c4c4',
      count: 1,
    },
    {
      label: 'Management',
      color: '#4242ff',
      count: 1,
    },
    {
      label: 'House Keeping',
      color: '#42f4e8',
      count: 1,
    },
    {
      label: 'Exp.Guest',
      color: '#eb2f96',
      count: 1,
    },
    {
      label: 'Block Dirty',
      color: '#e08888',
      count: 1,
    },
    {
      label: 'Settlement',
      color: '#808040',
      count: 1,
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
