import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomStatusInformationComponent } from './room-status-information.component';

describe('RoomStatusInformationComponent', () => {
  let component: RoomStatusInformationComponent;
  let fixture: ComponentFixture<RoomStatusInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoomStatusInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomStatusInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
