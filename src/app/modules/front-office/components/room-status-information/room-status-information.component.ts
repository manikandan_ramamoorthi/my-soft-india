import { Component, OnInit } from '@angular/core';
import { TRANSLATE_KEYS } from 'src/app/shared/constants/language';

@Component({
  selector: 'app-room-status-information',
  templateUrl: './room-status-information.component.html',
  styleUrls: ['./room-status-information.component.scss'],
})
export class RoomStatusInformationComponent implements OnInit {
  translateKey = TRANSLATE_KEYS.FRONT_OFFICE.STATUS_TAB;
  isLoading: boolean;
  roomInfo: any = [];
  dataByIndex: any = {
    floor_wise: [
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
    ],
    status_wise: [
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '129 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '125 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '131 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '154 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '115 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '129 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '125 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '131 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '154 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '115 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '129 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '125 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '131 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '154 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '115 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
    ],
    feature_wise: [
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '128 - DLX - tWIN BED', info: 'Siva', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '128 - DLX - tWIN BED', info: 'Siva', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '128 - DLX - tWIN BED', info: 'Siva', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
    ],
    block_wise: [
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
    ],
    type_wise: [
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '128 - DLX - tWIN BED', info: 'Siva', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '128 - DLX - tWIN BED', info: 'Siva', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '128 - DLX - tWIN BED', info: 'Siva', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
    ],
    floor_type_wise: [
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
      { name: '121 - DLX - tWIN BED', info: 'vacant', color: '#00d700' },
      { name: '122 - DLX - tWIN BED', info: 'Dirty', color: '#ffff2b' },
      { name: '123 - DLX - tWIN BED', info: 'Sarvesh', color: '#eb2f96' },
      {
        name: '124 - DLX - tWIN BED',
        info: 'mr srinivas - 2 - Rs. 10472.99',
        color: '#ff3e3e',
      },
      { name: '125 - DLX - tWIN BED', info: 'management', color: '#4242ff' },
      {
        name: '126 - DLX - tWIN BED',
        info: 'ggg - 4 - Rs. 920.00',
        color: '#808040',
      },
      {
        name: '127 - DLX - tWIN BED',
        info: 'HGGFGFD - 4 - Rs. 12867.98',
        color: '#bb00ff',
      },
      { name: '128 - DLX - tWIN BED', info: 'Bharathi', color: '#42f4e8' },
      { name: '129 - DLX - tWIN BED', info: 'maintenance', color: '#c4c4c4' },
    ],
  };
  tabs = [
    {
      name: 'floor_wise',
      icon: 'insert-row-right',
    },
    {
      name: 'status_wise',
      icon: 'info-circle',
    },
    {
      name: 'feature_wise',
      icon: 'fund-view',
    },
    {
      name: 'block_wise',
      icon: 'database',
    },
    {
      name: 'type_wise',
      icon: 'gateway',
    },
    {
      name: 'floor_type_wise',
      icon: 'group',
    },
  ];

  constructor() {
    this.isLoading = false;
  }

  handleSelectChange(event: any) {
    console.log(event.index, 'handleSelectChange');
  }

  handleTableClick({ name }: any) {
    this.roomInfo = this.dataByIndex[name];
  }

  ngOnInit(): void {
    this.roomInfo = this.dataByIndex['floor_wise'];
  }
}
