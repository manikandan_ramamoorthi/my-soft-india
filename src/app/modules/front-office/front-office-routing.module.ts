import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontOfficeComponent } from './front-office.component';
import { FrontOfficeHomeComponent } from './pages/front-office-home/front-office-home.component';
import { BookingComponent } from './pages/reservation/booking/booking.component';

const routes: Routes = [
  {
    path: '',
    component: FrontOfficeComponent,
    children: [
      { path: '', redirectTo: 'home' },
      { path: 'home', component: FrontOfficeHomeComponent },
      {
        path: 'reservation',
        children: [{ path: 'booking', component: BookingComponent }],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrontOfficeRoutingModule {}
