import { Actions } from './front-office-action';
import { IFrontOfficeState } from './front-office-model';

export const initialState: IFrontOfficeState = {};

export function FrontOfficeReducer(
  state = initialState,
  action: Actions
): IFrontOfficeState {
  return { ...state };
}
