import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { FrontOfficeRoutingModule } from './front-office-routing.module';
import { FrontOfficeComponent } from './front-office.component';
import { RoomAvailabilityComponent } from './components/room-availability/room-availability.component';
import { RoomInfoLegendComponent } from './components/room-info-legend/room-info-legend.component';
import { RoomStatusInformationComponent } from './components/room-status-information/room-status-information.component';
import { FrontOfficeHomeComponent } from './pages/front-office-home/front-office-home.component';
import { BookingComponent } from './pages/reservation/booking/booking.component';
@NgModule({
  declarations: [
    FrontOfficeComponent,
    RoomAvailabilityComponent,
    RoomInfoLegendComponent,
    RoomStatusInformationComponent,
    FrontOfficeHomeComponent,
    BookingComponent,
  ],
  imports: [SharedModule, FrontOfficeRoutingModule],
})
export class FrontOfficeModule {}
