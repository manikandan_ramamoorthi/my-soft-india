import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontOfficeHomeComponent } from './front-office-home.component';

describe('FrontOfficeHomeComponent', () => {
  let component: FrontOfficeHomeComponent;
  let fixture: ComponentFixture<FrontOfficeHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontOfficeHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontOfficeHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
