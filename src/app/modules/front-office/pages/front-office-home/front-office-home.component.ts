import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-front-office-home',
  templateUrl: './front-office-home.component.html',
  styleUrls: ['./front-office-home.component.scss'],
})
export class FrontOfficeHomeComponent implements OnInit {
  isStatusContainerVisible: boolean = false;
  isRoomAvailabilityModalVisible: boolean = false;

  constructor() {}

  // Room Availability Modal Close //
  handleStatusClose(event: boolean) {
    this.isStatusContainerVisible = event;
  }

  // Room Availability Modal Close //
  handleModalClose() {
    this.isRoomAvailabilityModalVisible = false;
  }

  ngOnInit(): void {}
}
