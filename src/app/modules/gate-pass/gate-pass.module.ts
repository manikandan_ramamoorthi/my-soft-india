import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { GatePassRoutingModule } from './gate-pass-routing.module';
import { GatePassComponent } from './gate-pass.component';
@NgModule({
  declarations: [GatePassComponent],
  imports: [SharedModule, GatePassRoutingModule],
})
export class GatePassModule {}
