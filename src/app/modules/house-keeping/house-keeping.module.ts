import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { HouseKeepingRoutingModule } from './house-keeping-routing.module';
import { HouseKeepingComponent } from './house-keeping.component';

@NgModule({
  declarations: [HouseKeepingComponent],
  imports: [SharedModule, HouseKeepingRoutingModule],
})
export class HouseKeepingModule {}
