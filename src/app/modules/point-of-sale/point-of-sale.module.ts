import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { PointOfSaleRoutingModule } from './point-of-sale-routing.module';
import { PointOfSaleComponent } from './point-of-sale.component';
@NgModule({
  declarations: [PointOfSaleComponent],
  imports: [SharedModule, PointOfSaleRoutingModule],
})
export class PointOfSaleModule {}
