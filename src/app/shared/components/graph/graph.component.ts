import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import * as c3 from 'c3';
import { DummyData, GraphConfig } from './graph.config';
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
})
export class GraphComponent implements OnInit, AfterViewInit {
  @Input() chartName: any;
  graphConfig: any;
  dummyData: any;
  isLoading: boolean;
  constructor() {
    this.graphConfig = GraphConfig;
    this.dummyData = DummyData;
    this.chartName = '';
    this.isLoading = true;
  }

  ngOnInit(): void {}

  generateChartData(data: {}, config: []): {} {
    return config.map((key) => {
      return [key, ...data[key]];
    });
  }

  generateChartConfig(): any {
    const config: any = this.graphConfig[this.chartName];
    const data = this.dummyData[this.chartName];
    console.log(this.generateChartData(data, config.columns));
    return {
      bindto: `#${this.chartName}`,
      data: {
        x: config.axis.x.key,
        columns: this.generateChartData(data, config.columns),
        type: config.type,
        colors: config.colors,
      },
      // size: config.size,
      axis: config.axis,
    };
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      let chart = c3.generate(this.generateChartConfig());
      this.isLoading = false;
    }, 350);
  }
}
