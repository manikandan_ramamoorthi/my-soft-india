export const GraphConfig = {
  roomsAvailability: {
    columns: [
      'time',
      'Dirty',
      'Occupied',
      'House Keeping',
      'Maintenance',
      'Vacant',
    ],
    // type: 'bar',
    type: 'spline',
    colors: {
      Dirty: '#6d848e',
      Occupied: '#febc3b',
      'House Keeping': '#68d4cd',
      Maintenance: '#fd8080',
      Vacant: '#26e7a6',
    },
    axis: {
      x: {
        type: 'timeseries',
        key: 'time',
        tick: {
          format: '%d-%m-%Y',
        },
      },
    },
  },
  todayRoomStatus: {
    columns: [
      'time',
      'Dirty',
      'Occupied',
      'House Keeping',
      'Maintenance',
      'Vacant',
    ],
    size: {
      height: 240,
      width: 380,
    },
    type: 'pie',
    colors: {
      Dirty: '#6d848e',
      Occupied: '#febc3b',
      'House Keeping': '#68d4cd',
      Maintenance: '#fd8080',
      Vacant: '#26e7a6',
    },
    axis: {
      x: {
        type: 'timeseries',
        key: 'time',
        tick: {
          format: '%d-%m-%Y',
        },
      },
    },
  },
  roomStatus: {
    columns: [
      'time',
      'Dirty',
      'Occupied',
      'House Keeping',
      'Maintenance',
      'Vacant',
    ],
    size: {
      height: 240,
      width: 850,
    },
    type: 'bar',
    colors: {
      Dirty: '#6d848e',
      Occupied: '#febc3b',
      'House Keeping': '#68d4cd',
      Maintenance: '#fd8080',
      Vacant: '#26e7a6',
    },
    axis: {
      x: {
        type: 'timeseries',
        key: 'time',
        tick: {
          format: '%d-%m-%Y',
        },
      },
    },
  },
};

export const DummyData = {
  roomsAvailability: {
    time: [
      '2021-02-20',
      '2021-02-21',
      '2021-02-22',
      '2021-02-23',
      '2021-02-24',
      '2021-02-25',
    ],
    Dirty: [10, 12, 2, 4, 15, 12],
    Occupied: [21, 12, 13, 18, 10, 7],
    'House Keeping': [7, 24, 15, 22, 13, 21],
    Maintenance: [12, 2, 20, 6, 12, 10],
    Vacant: [5, 18, 12, 11, 13, 21],
  },
  todayRoomStatus: {
    time: [
      '2021-02-20',
      '2021-02-21',
      '2021-02-22',
      '2021-02-23',
      '2021-02-24',
      '2021-02-25',
    ],
    Dirty: [10, 12, 2, 4, 15, 12],
    Occupied: [21, 12, 13, 18, 10, 7],
    'House Keeping': [7, 24, 15, 22, 13, 21],
    Maintenance: [12, 2, 20, 6, 12, 10],
    Vacant: [5, 18, 12, 11, 13, 21],
  },
  roomStatus: {
    time: [
      '2021-02-20',
      '2021-02-21',
      '2021-02-22',
      '2021-02-23',
      '2021-02-24',
      '2021-02-25',
    ],
    Dirty: [10, 12, 2, 4, 15, 12],
    Occupied: [21, 12, 13, 18, 10, 7],
    'House Keeping': [7, 24, 15, 22, 13, 21],
    Maintenance: [12, 2, 20, 6, 12, 10],
    Vacant: [5, 18, 12, 11, 13, 21],
  },
};
