export enum Size {
  ExtraSmall = '50px',
  Small = '100px',
  Medium = '150px',
  Large = '200px',
}

export enum Background {
  Black = '#00000066',
  White = '#ffffffaa',
  LightGrey = '#eaeaeaaa',
  Grey = '#bfbfbf66',
  Transparent = 'transparent',
  WhiteSolid = '#ffffff',
}

export enum Placement {
  Full = 'Full',
  Fill = 'Fill',
  Wrap = 'Wrap',
}
