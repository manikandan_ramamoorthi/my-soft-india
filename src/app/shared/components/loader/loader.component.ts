import { Component, Input, OnInit } from '@angular/core';
import { Background, Placement, Size } from './loader-interface';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
  @Input() size: Size;
  @Input() background: string;
  @Input() placement: string;
  @Input() zIndex: number;
  isLoading: boolean = true;
  constructor() {
    this.size = Size.Medium;
    this.background = Background.Grey;
    this.placement = Placement.Wrap;
    this.zIndex = 0;
  }

  ngOnInit(): void {}
}
