import { VISUAL_TYPES } from './simple-table-enum';
import { ISimpleTableConfig } from './simple-table-interface';

export const generateSimpleTableConfig = ({
  visualType = VISUAL_TYPES.TEXT,
  width = 100,
  colorType = 'default',
}): ISimpleTableConfig => {
  return {
    visualType,
    width,
    colorType,
  };
};
