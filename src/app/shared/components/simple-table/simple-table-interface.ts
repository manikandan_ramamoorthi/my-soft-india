export interface ISimpleTableConfig {
  visualType: string;
  width: number;
  colorType: string;
}

export interface IDimension {
  x: string | null;
  y: string | null;
}

export interface ISimpleTableFullConfig {
  title?: string;
  translateKey?: string;
  identityKey?: string;
  config?: { [key: string]: ISimpleTableConfig };
  staticData?: any;
  isBordered?: boolean;
  isPagination?: boolean;
  isCheckBox?: boolean;
  dimensions?: IDimension;
}
