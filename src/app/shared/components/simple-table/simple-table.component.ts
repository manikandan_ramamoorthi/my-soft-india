import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { COLOR_TEXT_CLASS, VISUAL_TYPES } from './simple-table-enum';
import { ISimpleTableFullConfig } from './simple-table-interface';

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.scss'],
})
export class SimpleTableComponent implements OnInit, OnChanges {
  @Input() tableConfig: any;
  @Input() tableData: any;
  sourceKeyColumns: any = [];
  visualTypesObj: any = VISUAL_TYPES;
  textColorClassObj: any = COLOR_TEXT_CLASS;
  constructor() {
    this.tableConfig = {};
    this.tableData = [];
  }

  ngOnInit(): void {
    // console.log(this.tableConfig, 'this.tableConfig');
  }

  ngOnChanges(changes: SimpleChanges) {
    const { tableConfig, tableData = [] } = changes;
    if (changes['tableConfig']) {
      const { currentValue, previousValue } = tableConfig;
      if (currentValue !== previousValue) {
        this.sourceKeyColumns = Object.keys(tableConfig?.currentValue?.config);
        // this.tableData = tableConfig.currentValue.staticData;
      }
    }
    if (changes['tableData']) {
      // console.log(tableData, 'tableData');
    }
  }
}
