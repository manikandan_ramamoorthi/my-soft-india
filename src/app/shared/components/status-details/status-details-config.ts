import { TABLE_IDENTITY_KEYS } from '../simple-table/simple-table-enum';
import { IStatusDetail } from './status-details-interface';

const {
  ROOM_STATUS_HOUSE_INFO,
  ROOM_STATUS_HOUSE_PLAN,
  ROOM_STATUS_EXPECTED_ARRIVALS,
  ROOM_STATUS_EXPECTED_DEPARTURE,
  ROOM_STATUS_TODAY_ARRIVALS,
  ROOM_STATUS_TODAY_CHECKOUT,
  ROOM_STATUS_TODAY_CLOSING,
} = TABLE_IDENTITY_KEYS;

export const ROOM_STATUS_ACCORDION = {};

export const StatusConfig: IStatusDetail = {
  title: 'Today Status',
  isAccordion: true,
  width: 400,
  accordionConfig: [
    {
      titleKey: 'house_count',
      infoKey: '12',
      active: true,
      childIdentities: [ROOM_STATUS_HOUSE_INFO, ROOM_STATUS_HOUSE_PLAN],
    },
    {
      titleKey: 'expected_arrivals',
      infoKey: '10',
      active: false,
      childIdentities: [ROOM_STATUS_EXPECTED_ARRIVALS],
    },
    {
      titleKey: 'expected_departure',
      infoKey: '15',
      active: false,
      childIdentities: [ROOM_STATUS_EXPECTED_DEPARTURE],
    },
    {
      titleKey: 'today_arrival',
      infoKey: '12',
      active: false,
      childIdentities: [ROOM_STATUS_TODAY_ARRIVALS],
    },
    {
      titleKey: 'today_checkout',
      infoKey: '7',
      active: false,
      childIdentities: [ROOM_STATUS_TODAY_CHECKOUT],
    },
    {
      titleKey: 'today_closing',
      infoKey: '9',
      active: false,
      childIdentities: [ROOM_STATUS_TODAY_CLOSING],
    },
  ],
};

export const TABLE_STATIC_DATA = {
  [ROOM_STATUS_HOUSE_INFO]: [
    {
      total_pax: '38',
      adult: '38',
      child: '51',
    },
  ],
  [ROOM_STATUS_HOUSE_PLAN]: [
    {
      plan: 'Meal Plan',
      ep: '2',
      cp: '36',
      map: '0',
      ap: 0,
    },
  ],
  [ROOM_STATUS_EXPECTED_ARRIVALS]: [
    {
      resv: '001',
      gst_name: 'Manikandan',
      gst_blk: '10',
      comp_name: 'IdeasToGoal',
      type: 'STD',
      rms: '',
      pax: '',
    },
  ],
  [ROOM_STATUS_EXPECTED_DEPARTURE]: [
    {
      room: '103',
      gst_name: 'Bharathi',
      plan: 'CP',
      amount: '11561.00',
    },
    {
      room: '104',
      gst_name: 'Siva',
      plan: 'CP',
      amount: '10561.00',
    },
    {
      room: '105',
      gst_name: 'Sarvesh',
      plan: 'CP',
      amount: '998.00',
    },
  ],
  [ROOM_STATUS_TODAY_ARRIVALS]: [
    {
      room: '106',
      gst_name: 'Bharathi',
      pax: '3',
      type: 'STD',
    },
    {
      room: '107',
      gst_name: 'Siva',
      pax: '2',
      type: 'STD',
    },
    {
      room: '108',
      gst_name: 'Sarvesh',
      pax: '2',
      type: 'STD',
    },
    {
      room: '109',
      gst_name: 'Sakthi',
      pax: '4',
      type: 'STD',
    },
  ],
  [ROOM_STATUS_TODAY_CHECKOUT]: [
    {
      room: '102',
      gst_name: 'Sarvesh',
      bill: 'FO1920-06459',
      amount: '2688',
    },
    {
      room: '104',
      gst_name: 'Siva',
      bill: 'FO1920-06460',
      amount: '1600',
    },
    {
      room: '114',
      gst_name: 'Bharathi',
      bill: 'FO1920-06463',
      amount: '1820',
    },
    {
      room: '118',
      gst_name: 'Sakthi Murgan',
      bill: 'FO1920-06464',
      amount: '1302',
    },
  ],
  [ROOM_STATUS_TODAY_CLOSING]: [],
};
