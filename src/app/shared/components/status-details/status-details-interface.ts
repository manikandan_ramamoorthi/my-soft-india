export interface IStatusDetail {
  title?: string;
  width?: number;
  isAccordion?: boolean;
  accordionConfig?: IStatusDetailChildConfig[];
}

export interface IStatusDetailChildConfig {
  titleKey?: string;
  infoKey?: string;
  dataKey?: string;
  active?: boolean;
  childIdentities?: Array<string>;
}
