import { TRANSLATE_KEYS } from '../../constants/language';
import { generateSimpleTableConfig } from '../simple-table/simple-table-config';
import { TABLE_IDENTITY_KEYS } from '../simple-table/simple-table-enum';
import { ISimpleTableFullConfig } from '../simple-table/simple-table-interface';
const { SHARED } = TRANSLATE_KEYS;

const DEFAULT_CONFIG: ISimpleTableFullConfig = {
  title: '',
  config: {},
  translateKey: SHARED.ROOM_STATUS_TABLE,
  identityKey: '',
  staticData: [],
  isBordered: true,
  isPagination: false,
  isCheckBox: false,
  dimensions: { x: null, y: null },
};

const HOUSE_INFO_CONFIG: ISimpleTableFullConfig = {
  ...DEFAULT_CONFIG,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_STATUS_HOUSE_INFO,
  config: {
    total_pax: generateSimpleTableConfig({}),
    adult: generateSimpleTableConfig({}),
    child: generateSimpleTableConfig({}),
  },
  staticData: [],
};

const HOUSE_PLAN_CONFIG: ISimpleTableFullConfig = {
  ...DEFAULT_CONFIG,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_STATUS_HOUSE_PLAN,
  config: {
    plan: generateSimpleTableConfig({}),
    ep: generateSimpleTableConfig({}),
    cp: generateSimpleTableConfig({}),
    map: generateSimpleTableConfig({}),
    ap: generateSimpleTableConfig({}),
  },
};

const EXPECTED_ARRIVALS_CONFIG = {
  ...DEFAULT_CONFIG,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_STATUS_EXPECTED_ARRIVALS,
  config: {
    resv: generateSimpleTableConfig({}),
    gst_name: generateSimpleTableConfig({}),
    gst_blk: generateSimpleTableConfig({}),
    comp_name: generateSimpleTableConfig({}),
    type: generateSimpleTableConfig({}),
    rms: generateSimpleTableConfig({}),
    pax: generateSimpleTableConfig({}),
  },
};

const EXPECTED_DEPARTURE_CONFIG = {
  ...DEFAULT_CONFIG,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_STATUS_EXPECTED_DEPARTURE,
  config: {
    room: generateSimpleTableConfig({}),
    gst_name: generateSimpleTableConfig({}),
    plan: generateSimpleTableConfig({}),
    amount: generateSimpleTableConfig({}),
  },
};

const TODAY_ARRIVALS_CONFIG = {
  ...DEFAULT_CONFIG,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_STATUS_TODAY_ARRIVALS,
  config: {
    room: generateSimpleTableConfig({}),
    gst_name: generateSimpleTableConfig({}),
    pax: generateSimpleTableConfig({}),
    type: generateSimpleTableConfig({}),
  },
};

const TODAY_CHECKOUT_CONFIG = {
  ...DEFAULT_CONFIG,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_STATUS_TODAY_CHECKOUT,
  config: {
    room: generateSimpleTableConfig({}),
    gst_name: generateSimpleTableConfig({}),
    bill: generateSimpleTableConfig({}),
    amount: generateSimpleTableConfig({}),
  },
};

const TODAY_CLOSING_CONFIG = {
  ...DEFAULT_CONFIG,
  identityKey: TABLE_IDENTITY_KEYS.ROOM_STATUS_TODAY_CLOSING,
  config: {
    room: generateSimpleTableConfig({}),
    gst_name: generateSimpleTableConfig({}),
    bill: generateSimpleTableConfig({}),
    amount: generateSimpleTableConfig({}),
  },
};

export const ROOM_STATUS_CONFIG_BY_IDENTITY = {
  [TABLE_IDENTITY_KEYS.ROOM_STATUS_HOUSE_INFO]: HOUSE_INFO_CONFIG,
  [TABLE_IDENTITY_KEYS.ROOM_STATUS_HOUSE_PLAN]: HOUSE_PLAN_CONFIG,
  [TABLE_IDENTITY_KEYS.ROOM_STATUS_EXPECTED_ARRIVALS]: EXPECTED_ARRIVALS_CONFIG,
  [TABLE_IDENTITY_KEYS.ROOM_STATUS_EXPECTED_DEPARTURE]: EXPECTED_DEPARTURE_CONFIG,
  [TABLE_IDENTITY_KEYS.ROOM_STATUS_TODAY_ARRIVALS]: TODAY_ARRIVALS_CONFIG,
  [TABLE_IDENTITY_KEYS.ROOM_STATUS_TODAY_CHECKOUT]: TODAY_CHECKOUT_CONFIG,
  [TABLE_IDENTITY_KEYS.ROOM_STATUS_TODAY_CLOSING]: TODAY_CLOSING_CONFIG,
};
