import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TRANSLATE_KEYS } from '../../constants/language';
import { TABLE_IDENTITY_KEYS } from '../simple-table/simple-table-enum';
import { StatusConfig, TABLE_STATIC_DATA } from './status-details-config';
import { ROOM_STATUS_CONFIG_BY_IDENTITY } from './status-details-table-config';
const {
  ROOM_STATUS_HOUSE_INFO,
  ROOM_STATUS_HOUSE_PLAN,
  ROOM_STATUS_EXPECTED_ARRIVALS,
  ROOM_STATUS_EXPECTED_DEPARTURE,
  ROOM_STATUS_TODAY_ARRIVALS,
  ROOM_STATUS_TODAY_CHECKOUT,
  ROOM_STATUS_TODAY_CLOSING,
} = TABLE_IDENTITY_KEYS;

@Component({
  selector: 'app-status-details',
  templateUrl: './status-details.component.html',
  styleUrls: ['./status-details.component.scss'],
})
export class StatusDetailsComponent implements OnInit {
  @Input() isVisible: boolean = true;
  @Input() detailConfig: any = StatusConfig;
  @Input() detailData: any = TABLE_STATIC_DATA;
  @Output() close = new EventEmitter<boolean>();
  tableConfigByIdentity: any = ROOM_STATUS_CONFIG_BY_IDENTITY;
  translateKey: string = TRANSLATE_KEYS.SHARED.ROOM_STATUS_ACCORDION;
  // statusInfo = [
  //   {
  //     name: 'house_count',
  //     count: 33,
  //     active: true,
  //     childConfig: [ROOM_STATUS_HOUSE_INFO, ROOM_STATUS_HOUSE_PLAN],
  //   },
  //   {
  //     name: 'expected_arrivals',
  //     count: 33,
  //     active: false,
  //     childConfig: [ROOM_STATUS_EXPECTED_ARRIVALS],
  //   },
  //   {
  //     name: 'expected_departure',
  //     count: 33,
  //     active: false,
  //     childConfig: [ROOM_STATUS_EXPECTED_DEPARTURE],
  //   },
  //   {
  //     name: 'today_arrival',
  //     count: 33,
  //     active: false,
  //     childConfig: [ROOM_STATUS_TODAY_ARRIVALS],
  //   },
  //   {
  //     name: 'today_checkout',
  //     count: 33,
  //     active: false,
  //     childConfig: [ROOM_STATUS_TODAY_CHECKOUT],
  //   },
  //   {
  //     name: 'today_closing',
  //     count: 33,
  //     active: false,
  //     childConfig: [ROOM_STATUS_TODAY_CLOSING],
  //     childData: [],
  //   },
  // ];
  constructor() {}

  ngOnInit(): void {}

  handleClose() {
    this.close.emit(false);
  }
}
