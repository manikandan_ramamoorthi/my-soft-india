import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubHeaderBarComponent } from './sub-header-bar.component';

describe('SubHeaderBarComponent', () => {
  let component: SubHeaderBarComponent;
  let fixture: ComponentFixture<SubHeaderBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubHeaderBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubHeaderBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
