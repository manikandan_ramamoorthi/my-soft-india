import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LayoutService } from 'src/app/layout/layout.service';

@Component({
  selector: 'app-sub-header-bar',
  templateUrl: './sub-header-bar.component.html',
  styleUrls: ['./sub-header-bar.component.scss'],
})
export class SubHeaderBarComponent implements OnInit {
  isCollapsed: boolean = false;
  subscriptionStack: Subscription[] = [];
  menuList: any = [
    {
      name: 'Home',
      route: '/home',
      isSubMenu: false,
      icon: 'home',
      child: [],
    },
    {
      name: 'Masters',
      route: '',
      isSubMenu: true,
      icon: 'info-circle',
      child: [
        {
          name: 'Master 1',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Business Source', route: '' },
            { name: 'Company Master', route: '' },
            { name: 'Currency', route: '' },
            { name: 'Ledgers', route: '' },
            { name: 'Meal Plans', route: '' },
          ],
        },
        {
          name: 'Master 2',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Market Segment', route: '' },
            { name: 'Nationality', route: '' },
            { name: 'Currency', route: '' },
            { name: 'Property Definition', route: '' },
            { name: 'Purpose of visit', route: '' },
            { name: 'Room Master', route: '' },
          ],
        },
        {
          name: 'Master 3',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Room Features', route: '' },
            { name: 'Rate Master', route: '' },
            { name: 'Room Types', route: '' },
            { name: 'Type of Billing', route: '' },
          ],
        },
        {
          name: 'Master 4',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Tax Types', route: '' },
            { name: 'Tax Structures', route: '' },
            { name: 'Tax Slab', route: '' },
          ],
        },
      ],
    },
    {
      name: 'Reservation',
      route: '',
      isSubMenu: true,
      icon: 'schedule',
      child: [
        { name: 'Booking', route: 'reservation/booking', isSubMenu: false },
        { name: 'Reservation Advance', route: '', isSubMenu: false },
        { name: 'Reservation/Copy', route: '', isSubMenu: false },
        { name: 'Pre Reg Card', route: '', isSubMenu: false },
        { name: 'Blank GuestReg Card', route: '', isSubMenu: false },
        { name: 'Reserv Adv Refund', route: '', isSubMenu: false },
        { name: 'Reinstate', route: '', isSubMenu: false },
        { name: 'Group Check-in', route: '', isSubMenu: false },
        { name: 'Quick Walkin check-in', route: '', isSubMenu: false },
        { name: 'Guest Block', route: '', isSubMenu: false },
        { name: 'Room Chart', route: '', isSubMenu: false },
      ],
    },
    {
      name: 'Transaction',
      route: '',
      isSubMenu: true,
      icon: 'transaction',
      child: [
        {
          name: 'Transaction 1',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Check In', route: '' },
            { name: 'Reservation Check-in', route: '' },
            { name: 'Reinstate Check-in', route: '' },
            { name: 'Room Advance', route: '' },
            { name: 'Tag Advance', route: '' },
          ],
        },
        {
          name: 'Transaction 2',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Change room', route: '' },
            { name: 'Block room', route: '' },
            { name: 'Paid Out', route: '' },
            { name: 'Charges', route: '' },
          ],
        },
        {
          name: 'Transaction 3',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Allowance', route: '' },
            { name: 'Check Out', route: '' },
            { name: 'Settlement', route: '' },
            { name: 'Night Audit', route: '' },
            { name: 'Clear Room', route: '' },
          ],
        },
        {
          name: 'Transaction 4',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Remove Tariff', route: '' },
            { name: 'Miscellaneous', route: '' },
            { name: 'Active/Deactive Folio', route: '' },
            { name: 'Log Book', route: '' },
          ],
        },
      ],
    },
    // {
    //   name: 'Accounts',
    //   route: '',
    //   isSubMenu: true,
    //   icon: 'idcard',
    //   child: [
    //     {
    //       name: 'Accounts 1',
    //       route: '',
    //       isSubMenu: true,
    //       child: [
    //         { name: 'Accounts receivable', route: '' },
    //         { name: 'Bill Matching', route: '' },
    //         { name: 'Bill Matching Report', route: '' },
    //         { name: 'Credit Settlement', route: '' },
    //         { name: 'Checkout Summary', route: '' },
    //         { name: 'Day Book', route: '' },
    //         { name: 'Tax Summary report', route: '' },
    //       ],
    //     },
    //     {
    //       name: 'Accounts 2',
    //       route: '',
    //       isSubMenu: true,
    //       child: [
    //         { name: 'GST BQT Advance', route: '' },
    //         { name: 'GST Amendments', route: '' },
    //         { name: 'GST Report', route: '' },
    //         { name: 'GST Reserv Advance', route: '' },
    //         { name: 'High Balance Report', route: '' },
    //       ],
    //     },
    //     {
    //       name: 'Accounts 3',
    //       route: '',
    //       isSubMenu: true,
    //       child: [
    //         { name: 'Outstanding Report', route: '' },
    //         { name: 'Opening Balance', route: '' },
    //         { name: 'Petty Cash', route: '' },
    //         { name: 'Petty Cash Ledger', route: '' },
    //         { name: 'Petty Cash Approval', route: '' },
    //         { name: 'Room Change', route: '' },
    //         { name: 'Recap Statement', route: '' },
    //         { name: 'Tally Mapping', route: '' },
    //       ],
    //     },
    //     {
    //       name: 'Accounts 4',
    //       route: '',
    //       isSubMenu: true,
    //       child: [
    //         { name: 'Tally Group', route: '' },
    //         { name: 'Tally Interface GL', route: '' },
    //         { name: 'Tally Interface CHK', route: '' },
    //         { name: 'UnAdjusted BQT Advance', route: '' },
    //       ],
    //     },
    //   ],
    // },
    {
      name: 'Reports',
      route: '',
      isSubMenu: true,
      icon: 'audit',
      child: [
        {
          name: 'Reports 1',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Arrival for the day', route: '' },
            { name: 'House Guest', route: '' },
            { name: 'Bill Summary', route: '' },
            { name: 'Cancellation Report', route: '' },
            { name: 'Company Analysis ', route: '' },
            { name: 'Clear Room report', route: '' },
            { name: 'Departure for the day', route: '' },
            { name: 'Daywise Tariff Disc', route: '' },
            { name: 'Detail Position', route: '' },
            { name: 'Cancelled Bills Report', route: '' },
          ],
        },
        {
          name: 'Reports 2',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Day Summary', route: '' },
            { name: 'Day Summary Monthly', route: '' },
            { name: 'Day Wise Occupancy', route: '' },
            { name: 'Expected Arrival', route: '' },
            { name: 'Expected Arrival View', route: '' },
            { name: 'Expected Departure', route: '' },
            { name: 'Eportal report', route: '' },
            { name: 'Eportal revenue report', route: '' },
            { name: 'FO Cash', route: '' },
            { name: 'Flash Report', route: '' },
            { name: 'Flash Report Checkout', route: '' },
          ],
        },
        {
          name: 'Reports 3',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Guest History', route: '' },
            { name: 'Guest Ledger', route: '' },
            { name: 'In-House Report', route: '' },
            { name: 'Image Capture Report', route: '' },
            { name: 'Long Stay guest', route: '' },
            { name: 'Link Room', route: '' },
            { name: 'Maintenance Report', route: '' },
            { name: 'Night Audit Report', route: '' },
            { name: 'No Show Report', route: '' },
            { name: 'Other Charges', route: '' },
          ],
        },
        {
          name: 'Reports 4',
          route: '',
          isSubMenu: true,
          child: [
            { name: 'Occupancy Details', route: '' },
            { name: 'Occupancy Forecast', route: '' },
            { name: 'Outstanding Report', route: '' },
            { name: 'Company Ledger', route: '' },
            { name: 'Room Typewise Sales', route: '' },
            { name: 'Remove Tariff Report', route: '' },
            { name: 'Room Charges', route: '' },
            { name: 'Room Charges month', route: '' },
            { name: 'Reservation Forecast', route: '' },
            { name: 'Reservation Log', route: '' },
            { name: 'Segment report', route: '' },
            { name: 'Nationality Details', route: '' },
          ],
        },
      ],
    },
    {
      name: 'Prints',
      route: '',
      isSubMenu: true,
      icon: 'printer',
      child: [
        { name: 'Check out Details', route: '', isSubMenu: false },
        { name: 'Reprint Bills', route: '', isSubMenu: false },
        { name: 'Duplicate Charges', route: '', isSubMenu: false },
        { name: 'Room Adv Details', route: '', isSubMenu: false },
        { name: 'Reserv Adv Details', route: '', isSubMenu: false },
        { name: 'Reserv Adv Refund', route: '', isSubMenu: false },
        { name: 'AR Receipts Details', route: '', isSubMenu: false },
        { name: 'Settle Duplicate', route: '', isSubMenu: false },
        { name: 'Laundry Duplicate', route: '', isSubMenu: false },
        { name: 'Allowance Duplicate', route: '', isSubMenu: false },
        { name: 'Miscellaneous Duplicate', route: '', isSubMenu: false },
        { name: 'Paid Out', route: '', isSubMenu: false },
      ],
    },
  ];
  constructor(private layoutService: LayoutService, private router: Router) {}

  navigate(routePath: string) {
    if (routePath) {
      this.router.navigate(['front-office/' + routePath]);
    }
  }

  ngOnInit(): void {
    const navigationViewStateSub = this.layoutService.navigationViewState.subscribe(
      (viewState) => (this.isCollapsed = viewState)
    );
    this.subscriptionStack.push(navigationViewStateSub);
  }
  ngOnDestroy() {
    this.subscriptionStack.forEach((sub) => sub.unsubscribe());
  }
}
