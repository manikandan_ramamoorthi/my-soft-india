const DASHBOARD = {};
const ADMIN = {};
const FRONT_OFFICE = {};
const BANQUETS = {};
const FOOD_COSTING = {};
const GATE_PASS = {};
const HOUSE_KEEPING = {};
const MAINTENANCE = {};
const MATERIAL = {};
const PAYROLL = {};
const POINT_OF_SALE = {};

export const API_END_POINTS = {
  DASHBOARD,
  ADMIN,
  FRONT_OFFICE,
  BANQUETS,
  FOOD_COSTING,
  GATE_PASS,
  HOUSE_KEEPING,
  MAINTENANCE,
  PAYROLL,
  MATERIAL,
  POINT_OF_SALE,
};
