export const LANGUAGES = ['ta', 'en'];
export const LANG_KEY = 'LANG_KEY';

export const TRANSLATE_KEYS = {
  DASHBOARD: {
    INFO_CARD: 'dashboard.info_card.',
  },
  NAVIGATION: {
    SIDE_BAR: 'navigation.sidebar.',
    HEADER_BAR: 'navigation.header_bar.',
  },
  FRONT_OFFICE: {
    STATUS_TAB: 'front_office.status_tab.',
    ROOM_AVAILABLE_STATUS_TABLE: 'front_office.room_available_status_table.',
  },
  ADMIN: {},
  BANQUETS: {},
  FOOD_COSTING: {},
  GATE_PASS: {},
  HOUSE_KEEPING: {},
  MAINTENANCE: {},
  MATERIAL: {},
  PAYROLL: {},
  POINT_OF_SALE: {},
  SHARED: {
    ROOM_STATUS_TABLE: 'shared.room_status_table.',
    ROOM_STATUS_ACCORDION: 'shared.room_status_accordion.',
  },
};
