import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import en from '@angular/common/locales/en';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IconsProviderModule } from '../icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LocalStorageUtils } from './utils/localstorage-utils';
import { LANG_KEY } from './constants/language';
import { SubHeaderBarComponent } from './components/sub-header-bar/sub-header-bar.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { SimpleTableComponent } from './components/simple-table/simple-table.component';
import { DataTableComponent } from './components/data-table/data-table.component';
import { StatusDetailsComponent } from './components/status-details/status-details.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { GraphComponent } from './components/graph/graph.component';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { LoaderComponent } from './components/loader/loader.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzBadgeModule } from 'ng-zorro-antd/badge';

@NgModule({
  declarations: [
    SubHeaderBarComponent,
    SimpleTableComponent,
    DataTableComponent,
    StatusDetailsComponent,
    GraphComponent,
    LoaderComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    FormsModule,
    HttpClientModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    NzBreadCrumbModule,
    NzGridModule,
    NzCardModule,
    NzAvatarModule,
    NzResultModule,
    NzButtonModule,
    NzSelectModule,
    NzDropDownModule,
    NzTabsModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzCheckboxModule,
    NzDividerModule,
    NzTableModule,
    NzCollapseModule,
    NzDrawerModule,
    NzDescriptionsModule,
    NzModalModule,
    NzDatePickerModule,
    NzSpaceModule,
    NzToolTipModule,
    NzSpinModule,
    NzTimePickerModule,
    NzPopoverModule,
    NzBadgeModule,
  ],
  exports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    HttpClientModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    NzBreadCrumbModule,
    NzGridModule,
    NzCardModule,
    NzAvatarModule,
    NzResultModule,
    NzButtonModule,
    NzSelectModule,
    NzDropDownModule,
    NzTabsModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzCheckboxModule,
    NzDividerModule,
    NzTableModule,
    NzCollapseModule,
    NzDrawerModule,
    NzDescriptionsModule,
    NzModalModule,
    NzDatePickerModule,
    NzSpaceModule,
    NzToolTipModule,
    NzSpinModule,
    NzTimePickerModule,
    NzPopoverModule,
    NzBadgeModule,
    // components //
    SubHeaderBarComponent,
    SimpleTableComponent,
    StatusDetailsComponent,
    GraphComponent,
    LoaderComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SharedModule {
  constructor(translate: TranslateService) {
    const lang = LocalStorageUtils.getValue(LANG_KEY) || 'en';
    translate.setDefaultLang(lang);
    registerLocaleData(en);
    translate.use(lang);
  }
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
