const setValue = (key: string, value: any) => localStorage.setItem(key, value);

const getValue = (key: string) => localStorage.getItem(key) || '';

export const LocalStorageUtils = {
  setValue,
  getValue,
};
